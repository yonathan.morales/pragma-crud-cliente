package com.pragma.crudclient;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@AutoConfigureMockMvc
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CrudClientApplicationTests {

	@Autowired
	MockMvc mock;

	@Test
	@Order(0)
	void testClientById() throws Exception {
		mock.perform(get("/pragma-client/api/client/by-document-number")
						.content("{\"123\"}")
				).andDo(print());
	}

}
