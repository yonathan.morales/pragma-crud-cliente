package com.pragma.crudclient.web.controller;

import com.pragma.crudclient.domain.Client;
import com.pragma.crudclient.domain.Response;
import com.pragma.crudclient.domain.dto.ClientDto;
import com.pragma.crudclient.domain.idto.IClientDto;
import com.pragma.crudclient.domain.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/client")
public class ClientController {
    private final ClientService service;

    @Autowired
    public ClientController(ClientService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<Response> save(@RequestBody ClientDto dto) {
        Client client = new Client(dto);
        Response response = service.save(client);
        return new ResponseEntity<>(response, response.isSuccessful() ? HttpStatus.CREATED : HttpStatus.NOT_FOUND);
    }

    @GetMapping("/by-document-number/{documentNumber}")
    public ResponseEntity<Response> getByDocumentNumber(@PathVariable("documentNumber") String documentNumber) {
        return new ResponseEntity<>(service.getByDocumentNumber(documentNumber), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Response> update(@RequestBody ClientDto dto) {
        Client client = new Client(dto);
        Response response = service.update(client);
        return new ResponseEntity<>(response, response.isSuccessful() ? HttpStatus.CREATED : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{idClient}")
    public ResponseEntity<Response> delete(@PathVariable int idClient) {
        Response response = service.delete(idClient);
        return new ResponseEntity<>(response, response.isSuccessful() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    /*@GetMapping("/age-range/{dateStart}/{dateEnd}")
    public ResponseEntity<List<ClientDto>> getAgeRange(@PathVariable("dateStart") String dateStart, @PathVariable("dateEnd") String dateEnd) {
        return new ResponseEntity<>(service.getAgeRange(dateStart, dateEnd)
                .orElse(Collections.emptyList()), HttpStatus.OK);
    }*/

    @GetMapping("/age-range/{dateStart}/{dateEnd}")
    public ResponseEntity<List<IClientDto>> getAgeRange(@PathVariable("dateStart") String dateStart, @PathVariable("dateEnd") String dateEnd) {
        return new ResponseEntity<>(service.getAgeRange(dateStart, dateEnd), HttpStatus.OK);
    }
}
