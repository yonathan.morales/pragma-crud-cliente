package com.pragma.crudclient.web.exception;

import com.pragma.crudclient.domain.exception.BaseException;
import com.pragma.crudclient.domain.exception.InvalidValueException;
import com.pragma.crudclient.domain.exception.MandatoryValueException;
import com.pragma.crudclient.domain.exception.NoDataException;
import com.pragma.crudclient.domain.exception.validation.DomainValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.concurrent.ConcurrentHashMap;

@RestControllerAdvice
public class RestExceptionHandler {
    private static final ConcurrentHashMap<String, HttpStatus> STATES = new ConcurrentHashMap<>();

    public RestExceptionHandler() {
        STATES.put(InvalidValueException.class.getSimpleName(), HttpStatus.UNPROCESSABLE_ENTITY);
        STATES.put(NoDataException.class.getSimpleName(), HttpStatus.NOT_FOUND);
        STATES.put(MandatoryValueException.class.getSimpleName(), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(BaseException.class)
    public ResponseEntity<Error> exceptionResolver(BaseException e) {
        HttpStatus status = STATES.get(e.getClass().getSimpleName());
        Error err = new Error(e.getClass().getSimpleName(), e.getTechnicalMessage(), e.getHumanMessage());

        return ResponseEntity.status(status).body(err);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Error> exceptionResolver(Exception e) {
        Error err = new Error(e.getClass().getSimpleName(), e.getMessage(), DomainValidator.DEFAULT_MESSAGE);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(err);
    }
}
