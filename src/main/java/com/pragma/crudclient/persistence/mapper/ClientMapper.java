package com.pragma.crudclient.persistence.mapper;

import com.pragma.crudclient.domain.Client;
import com.pragma.crudclient.domain.dto.ClientDto;
import com.pragma.crudclient.persistence.entity.Cliente;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ClientMapper {
    @Mapping(source = "idCliente", target = "idClient")
    @Mapping(source = "nombre", target = "name")
    @Mapping(source = "apellido", target = "lastname")
    @Mapping(source = "tipDocumento", target = "documentType")
    @Mapping(source = "nroDocumento", target = "documentNumber")
    @Mapping(source = "fchNacimiento", target = "birthDate")
    @Mapping(source = "ciudadNacimiento", target = "birthCity")
    ClientDto toClientDto(Cliente cliente);
    List<ClientDto> toClientsDto(List<Cliente> clientes);

    @Mapping(target = "idCliente", source = "idClient")
    @Mapping(target = "nombre", source = "name")
    @Mapping(target = "apellido", source = "lastname")
    @Mapping(target = "tipDocumento", source = "documentType")
    @Mapping(target = "nroDocumento", source = "documentNumber")
    @Mapping(target = "fchNacimiento", source = "birthDate")
    @Mapping(target = "ciudadNacimiento", source = "birthCity")
    Cliente toCliente(Client client);
}
