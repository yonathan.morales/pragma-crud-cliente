package com.pragma.crudclient.persistence;

import com.pragma.crudclient.domain.Client;
import com.pragma.crudclient.domain.dto.ClientDto;
import com.pragma.crudclient.domain.idto.IClientDto;
import com.pragma.crudclient.domain.repository.ClientRepository;
import com.pragma.crudclient.persistence.crud.CrudClientRepository;
import com.pragma.crudclient.persistence.entity.Cliente;
import com.pragma.crudclient.persistence.mapper.ClientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public class ClientRepositoryImpl implements ClientRepository {
    private final CrudClientRepository crud;
    private final ClientMapper mapper;

    @Autowired
    public ClientRepositoryImpl(CrudClientRepository crud, ClientMapper mapper) {
        this.crud = crud;
        this.mapper = mapper;
    }

    @Override
    public ClientDto save(Client client) {
        Cliente cliente = mapper.toCliente(client);
        cliente.setIdCliente(crud.getNewIdClient());
        return mapper.toClientDto(crud.save(cliente));
    }

    @Override
    public Optional<ClientDto> getByDocumentNumber(String documentNumber) {
        Optional<Cliente> cliente = crud.findByNroDocumento(documentNumber);
        return cliente.map(mapper::toClientDto);
    }

    @Override
    @Transactional
    public void update(Client client) {
        Cliente cliente = getById(client.getIdClient());
        cliente.setNombre(client.getName());
        cliente.setApellido(client.getLastname());
        cliente.setTipDocumento(client.getDocumentType());
        cliente.setNroDocumento(client.getDocumentNumber());
        cliente.setCiudadNacimiento(client.getBirthCity());
        cliente.setFchNacimiento(client.getBirthDate());
        crud.save(cliente);
    }

    @Override
    public void delete(int idClient) {
        crud.deleteById(idClient);
    }

    @Override
    public Cliente getById(Integer idClient) {
        return crud.findById(idClient).orElse(null);
    }

    @Override
    public boolean exists(int idClient) {
        return crud.existsById(idClient);
    }

    /*@Override
    public Optional<List<ClientDto>> getAgeRange(LocalDate dateStart, LocalDate dateEnd) {
        Optional<List<Cliente>> clientes = crud.findByFchNacimientoBetween(dateStart, dateEnd);
        return clientes.map(mapper::toClientsDto);
    }*/

    @Override
    public List<IClientDto> getAgeRange(LocalDate dateStart, LocalDate dateEnd) {
        return crud.findByFchNacimientoBetween(dateStart, dateEnd);
    }
}
