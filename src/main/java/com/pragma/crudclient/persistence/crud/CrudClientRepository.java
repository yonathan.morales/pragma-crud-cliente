package com.pragma.crudclient.persistence.crud;

import com.pragma.crudclient.domain.idto.IClientDto;
import com.pragma.crudclient.persistence.entity.Cliente;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CrudClientRepository extends CrudRepository<Cliente, Integer> {

    @Query(value = "SELECT IFNULL(MAX(id_cliente) + 1, 1) FROM cliente", nativeQuery = true)
    int getNewIdClient();

    Optional<Cliente> findByNroDocumento(String documentNumber);
    //Optional<List<Cliente>> findByFchNacimientoBetween(LocalDate dateStart, LocalDate dateEnd);

    /*@Query(value = "select * " +
            "       from    cliente " +
            "       where   fch_nacimiento between :dateStart and :dateEnd", nativeQuery = true)
    Optional<List<Cliente>> findByFchNacimientoBetween(@Param("dateStart") LocalDate dateStart,
                                                       @Param("dateEnd") LocalDate dateEnd);*/

    @Query(value = "select id_cliente as idClient, nombre as name, apellido as lastname, tip_documento as documentType, " +
            "       nro_documento as documentNumber, ciudad_nacimiento as birthCity, fch_nacimiento as birthDate " +
            "       from    cliente " +
            "       where   fch_nacimiento between :dateStart and :dateEnd", nativeQuery = true)
    List<IClientDto> findByFchNacimientoBetween(@Param("dateStart") LocalDate dateStart,
                                                 @Param("dateEnd") LocalDate dateEnd);
}
