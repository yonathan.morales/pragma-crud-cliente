package com.pragma.crudclient.persistence.entity;

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;

@Data
public class Foto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;
    private String foto;
}
