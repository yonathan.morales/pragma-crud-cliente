package com.pragma.crudclient.persistence.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "cliente")
@Data
public class Cliente {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cliente")
    private Integer idCliente;

    private String nombre;

    private String apellido;

    @Column(name = "tip_documento")
    private String tipDocumento;

    @Column(name = "nro_documento")
    private String nroDocumento;

    @Column(name = "ciudad_nacimiento")
    private String ciudadNacimiento;

    @Column(name = "fch_nacimiento")
    private LocalDate fchNacimiento;
}
