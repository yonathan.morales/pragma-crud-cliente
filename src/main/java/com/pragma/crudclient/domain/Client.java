package com.pragma.crudclient.domain;

import com.pragma.crudclient.domain.dto.ClientDto;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static com.pragma.crudclient.domain.exception.validation.DomainValidator.*;

@Getter
@Setter
public class Client {
    private Integer idClient;
    private String name;
    private String lastname;
    private String documentType;
    private String documentNumber;
    private String birthCity;
    private LocalDate birthDate;

    private static final String ID_CLIENT = "id del cliente";
    private static final String NAMES = "nombre del cliente";
    private static final String AGE_INVALID_MESSAGE = "La fecha de nacimiento no puede ser mayor a la fecha actual";

    private static final List<String> DOCUMENT_TYPES = Arrays.asList("CC", "CE", "NIT", "RC", "TI");
    private static final String DOCUMENT_TYPES_DONT_EXIST = "El tipo de documento es inválido. Deber ser CC, CE, NIT, RC o TI";

    public Client(ClientDto dto) {
        validateMandatory(dto.getIdClient(), mandatoryMessage(ID_CLIENT), DEFAULT_MESSAGE);
        validateMandatory(dto.getName(), mandatoryMessage(NAMES), DEFAULT_MESSAGE);
        validateContains(dto.getDocumentType(), DOCUMENT_TYPES, DOCUMENT_TYPES_DONT_EXIST, DEFAULT_MESSAGE);
        validateAfter(LocalDate.now(), dto.getBirthDate(), AGE_INVALID_MESSAGE, DEFAULT_MESSAGE);

        this.idClient = dto.getIdClient();
        this.name = dto.getName();
        this.lastname = dto.getLastname();
        this.documentType = dto.getDocumentType();
        this.documentNumber = dto.getDocumentNumber();
        this.birthCity = dto.getBirthCity();
        this.birthDate = dto.getBirthDate();
    }
}
