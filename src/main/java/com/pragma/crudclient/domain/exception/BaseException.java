package com.pragma.crudclient.domain.exception;

import lombok.Getter;

@Getter
public abstract class BaseException extends RuntimeException {
    private String technicalMessage;
    private String humanMessage;

    public BaseException(String technicalMessage, String humanMessage) {
        super(technicalMessage);
        this.technicalMessage = technicalMessage;
        this.humanMessage = humanMessage;
    }
}
