package com.pragma.crudclient.domain.exception.validation;

import com.pragma.crudclient.domain.exception.*;

import java.time.LocalDate;
import java.util.List;

public class DomainValidator {
    public static final String FIELD_MANDATORY = "El campo %s es obligatorio.";
    public static final String DEFAULT_MESSAGE = "Ocurrió un error procesando la solicitud. Por favor contacta al administrador del sistema.";

    private DomainValidator() {}

    public static void validateMandatory(Object value, String technicalMessage, String humanMessage) {
        if (value == null || (value instanceof String && ((String) value).trim().isEmpty())) {
            throw new MandatoryValueException(technicalMessage, humanMessage);
        }
    }

    public static void validateAfter(LocalDate dateToBeAfter, LocalDate dateToBeBefore, String technicalMessage, String humanMessage) {
        if (dateToBeAfter != null && dateToBeBefore != null && dateToBeAfter.isBefore(dateToBeBefore)) {
            throw new InvalidValueException(technicalMessage, humanMessage);
        }
    }

    public static <T> void validateContains(T value, List<T> list, String technicalMessage, String humanMessage) {
        if (list != null && !list.contains(value)) {
            throw new InvalidValueException(technicalMessage, humanMessage);
        }
    }

    public static String formattedMessage(String baseMessage, Object... values) {
        return String.format(baseMessage, values);
    }

    public static String mandatoryMessage(String field) {
        return formattedMessage(FIELD_MANDATORY, field);
    }
}