package com.pragma.crudclient.domain;

import com.pragma.crudclient.domain.dto.ClientDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {
    private boolean successful;
    private String message;
    private ClientDto client;
}
