package com.pragma.crudclient.domain.idto;

import java.time.LocalDate;

public interface IClientDto {

    Integer getIdClient();
    String getName();
    String getLastname();
    String getDocumentType();
    String getDocumentNumber();
    String getBirthCity();
    LocalDate getBirthDate();
}
