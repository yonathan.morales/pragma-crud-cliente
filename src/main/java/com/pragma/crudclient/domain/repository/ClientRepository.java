package com.pragma.crudclient.domain.repository;

import com.pragma.crudclient.domain.Client;
import com.pragma.crudclient.domain.dto.ClientDto;
import com.pragma.crudclient.domain.idto.IClientDto;
import com.pragma.crudclient.persistence.entity.Cliente;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ClientRepository {
    ClientDto save(Client client);
    Optional<ClientDto> getByDocumentNumber(String documentNumber);
    void update(Client client);
    void delete(int idClient);
    Cliente getById(Integer idClient);
    boolean exists(int idClient);
    //Optional<List<ClientDto>> getAgeRange(LocalDate dateStart, LocalDate dateEnd);

    List<IClientDto> getAgeRange(LocalDate dateStart, LocalDate dateEnd);
}
