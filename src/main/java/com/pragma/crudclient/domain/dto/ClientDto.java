package com.pragma.crudclient.domain.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ClientDto {
    private Integer idClient;
    private String name;
    private String lastname;
    private String documentType;
    private String documentNumber;
    private String birthCity;
    private LocalDate birthDate;
}
