package com.pragma.crudclient.domain.service;

import com.pragma.crudclient.domain.Client;
import com.pragma.crudclient.domain.Response;
import com.pragma.crudclient.domain.dto.ClientDto;
import com.pragma.crudclient.domain.exception.NoDataException;
import com.pragma.crudclient.domain.idto.IClientDto;
import com.pragma.crudclient.domain.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.pragma.crudclient.domain.exception.validation.DomainValidator.DEFAULT_MESSAGE;
import static com.pragma.crudclient.domain.exception.validation.DomainValidator.validateAfter;

@Service
public class ClientService {
    private final ClientRepository repository;

    private static final String CLIENT_EXISTS_MESSAGE = "El cliente existe.";
    private static final String CLIENT_NOT_EXISTS_MESSAGE = "El cliente no existe en la base de datos.";
    private static final String CLIENT_NOT_EXISTS_TECH_MESSAGE = "El idClient %d no existe en la base de datos.";
    private static final String CLIENT_UPDATED_MESSAGE = "EL cliente se actualizó correctamente";
    private static final String CLIENT_CREATED_MESSAGE = "EL cliente se creó correctamente";
    private static final String CLIENT_DELETE_MESSAGE = "EL cliente se eliminó correctamente";
    private static final String DATES_INVALID_MESSAGE = "La fecha de inicio no puede ser mayor a la fecha fin";

    @Autowired
    public ClientService(ClientRepository repository) {
        this.repository = repository;
    }

    public Response save(Client client) {
        Response response = getByDocumentNumber(client.getDocumentNumber());
        if(!response.isSuccessful()) {
            ClientDto dto = repository.save(client);
            response.setSuccessful(true);
            response.setMessage(CLIENT_CREATED_MESSAGE);
            response.setClient(dto);
        } else {
            response.setSuccessful(false);
        }
        return response;
    }

    public Response getByDocumentNumber(String documentNumber) {
        Response response = new Response();
        Optional<ClientDto> dto = repository.getByDocumentNumber(documentNumber);
        if(dto.isPresent()) {
            response.setSuccessful(true);
            response.setMessage(CLIENT_EXISTS_MESSAGE);
            response.setClient(dto.get());
        } else {
            response.setMessage(CLIENT_NOT_EXISTS_MESSAGE);
        }
        return response;
    }

    public Response update(Client client) {
        validateClientExists(client.getIdClient());
        Response resp = new Response();
        repository.update(client);
        resp.setSuccessful(true);
        resp.setMessage(CLIENT_UPDATED_MESSAGE);
        return resp;
    }

    public Response delete(int idClient) {
        validateClientExists(idClient);
        Response resp = new Response();
        repository.delete(idClient);
        resp.setSuccessful(true);
        resp.setMessage(CLIENT_DELETE_MESSAGE);
        return resp;
    }

    private void validateClientExists(int idClient) {
        if(!repository.exists(idClient)) {
            throw new NoDataException(String.format(CLIENT_NOT_EXISTS_TECH_MESSAGE, idClient), CLIENT_NOT_EXISTS_MESSAGE);
        }
    }

    /*public Optional<List<ClientDto>> getAgeRange(String dateStart, String dateEnd) {
        validateAfter(LocalDate.parse(dateEnd), LocalDate.parse(dateStart), DATES_INVALID_MESSAGE, DEFAULT_MESSAGE);
        return repository.getAgeRange(LocalDate.parse(dateStart), LocalDate.parse(dateEnd));
    }*/

    public List<IClientDto> getAgeRange(String dateStart, String dateEnd) {
        validateAfter(LocalDate.parse(dateEnd), LocalDate.parse(dateStart), DATES_INVALID_MESSAGE, DEFAULT_MESSAGE);
        return repository.getAgeRange(LocalDate.parse(dateStart), LocalDate.parse(dateEnd));
    }
}
